<?php
$config = array(
    'db' => array(
        'host' => 'pdflist.sqlite',
        'name' => 'pdflist'
    )
);

// Se il database non esiste, deve essere creato.
try {
    
    $db = new PDO('sqlite:'.$config['db']['host']);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->prepare('DELECT name FROM people');
    // Chiudi la connessione:
    // $db = null;
} catch (PDOException $e) {
    echo 'Database error:'.PHP_EOL."\t".$e->getMessage().PHP_EOL;
}