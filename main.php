<?php
require_once(__DIR__.'/vendor/autoload.php');

use RedBean_Facade as R;

R::setup('sqlite:pdflist.sqlite');
$post = R::dispense('post');
$post->text = 'Hello World';

$id = R::store($post);
$post = R::load('post', $id);
print_r($post);
R::trash($post);